import './App.css';
import Navbar from './Components/Navbar/Navbar';
import PaymentStatus from './Components/Payment/PaymentStatus'
import ApprovalList from './Components/Approval/ApprovalList';
import EntryForm from './Components/Entry/EntryForm';
import PendingStatus from './Components/Pending/PendingStatus';
import Settings from './Components/Settings/Settings';
import SummaryReport from './Components/Summary/SummaryReport'
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Sidebar from './Components/Sidebar/Sidebar';


function App() {
  return (
      <Router>
        <Navbar/> 
        <div style={{display :'flex'}}>
    <Sidebar/>
    <div style={{marginTop : '45px', marginLeft: '17vw'}}>
        <Routes>
        <Route exact path='/' element={<EntryForm/>}/>
          <Route exact path='/payment' element={<PaymentStatus  />}/>
          <Route exact path='/approval' element={<ApprovalList/>}/>
          <Route exact path='/pending' element={<PendingStatus/>}/>
          <Route exact path='/settings' element={<Settings/>}/>
          <Route exact path='/summary' element={<SummaryReport/>}/>
        </Routes>
        </div>
        </div>
      </Router>
  );
}

export default App;