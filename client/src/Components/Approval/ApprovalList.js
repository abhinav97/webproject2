import {Table, TableHead, TableRow, TableBody, TableCell, styled} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import {userData} from './Data'; 
import {useState} from 'react';
import ReactPaginate from 'react-paginate';
import './approval.css';

const StyledTable= styled(Table)`
marginLeft : '10px';
font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
border :1px solid #dddddd;
margin-top : 20px;
`;

const THead = styled(TableRow)`
    & > th{
    font-size : 18px;
    font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
    font-weight : 600;
    border :1px solid #dddddd;
    }
`;

const THead2 = styled(TableRow)`
& > th{
  font-size : 15px;
  font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
  font-weight : 600;
  border :1px solid #dddddd;
  background-color: #dddddd;
  }
`

const StyledBody  = styled(TableBody)`
    & > tr >td{
      font-size : 15px;
      font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
      font-weight : 600;
      border :1px solid #dddddd;
    }
      & > tr:nth-child(even) {
      background-color: #dddddd;
    }
`;

const ApprovalList = () => {

  const [users, Setusers] = useState(userData.slice(0, 50));

  const [pageNumber, setPageNumber]=useState(0);

  const userPerPage = 5;

  const pagesVisited = pageNumber*userPerPage ;

  const displayData = users.slice(pagesVisited, pagesVisited + userPerPage);

  const pageCount = Math.ceil(users.length/userPerPage);

  const changePage = ({selected})=> {
    setPageNumber(selected)
  }

  return (
    <div>
      <h4 style={{marginLeft : '10px' ,color : '#2F4050', marginTop : '15px'}}>Approval List</h4>
     <StyledTable>
  <TableHead>
    <THead>
    <TableCell>Actions</TableCell>
    <TableCell>Approve</TableCell>
    <TableCell>Status</TableCell>
    <TableCell>Year</TableCell>
    <TableCell>Month</TableCell>
    <TableCell>Document No</TableCell>
    <TableCell>Document Date</TableCell>
    <TableCell>Total Amount</TableCell>
    <TableCell>SS Code</TableCell>
    <TableCell>Username</TableCell>
    <TableCell>User Remarks</TableCell>
    </THead>
  <THead2>
    <TableCell></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
  </THead2>
  </TableHead>
<StyledBody>
  {
    displayData.map((user, id)=>(
  <TableRow key={user.id}>
  
    <TableCell><EditIcon style={{marginRight : '10px'}}/><DeleteIcon/></TableCell>
    <TableCell>{user.approve}</TableCell>
    <TableCell>{user.status}</TableCell>
    <TableCell>{user.year}</TableCell>
    <TableCell>{user.month}</TableCell>
    <TableCell>{user.documentNumber}</TableCell>
    <TableCell>{user.documentDate}</TableCell>
    <TableCell>{user.totalAmount}</TableCell>
    <TableCell>{user.ssCode}</TableCell>
    <TableCell>{user.userName}</TableCell>
    <TableCell>{user.userRemarks}</TableCell>
  </TableRow>
  ))
}
</StyledBody>
 
</StyledTable>
<ReactPaginate
previousLabel={'Previous'}
nextLabel={'Next'}
pageCount={pageCount}
onPageChange={changePage}
containerClassName={'paginationBttns'}
previousLinkClassName={'previousBttn'}
nextLinkClassName={'nextBttn'}
disabledClassName={'paginationDisabled'}
activeClassName = {'pagnationActive'}
/>
    </div>
  )
}

export default ApprovalList
     