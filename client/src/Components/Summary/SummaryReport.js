import {Table, TableHead, TableRow, TableBody, TableCell, styled} from '@mui/material';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {userData} from './data'; 
import {useState} from 'react';
import ReactPaginate from 'react-paginate';
import './summary.css';


const StyledTable= styled(Table)`
marginLeft : '10px';
font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
border :1px solid #dddddd;
margin-top : 20px;
`;

const THead = styled(TableRow)`
    & > th{
    font-size : 18px;
    font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
    font-weight : 600;
    border :1px solid #dddddd;
    }
`;

const THead2 = styled(TableRow)`
& > th{
  font-size : 15px;
  font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
  font-weight : 600;
  border :1px solid #dddddd;
  background-color: #dddddd;
  }
`

const StyledBody  = styled(TableBody)`
    & > tr >td{
      font-size : 15px;
      font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
      font-weight : 600;
      border :1px solid #dddddd;
    }
      & > tr:nth-child(even) {
      background-color: #dddddd;
    }
`;

const SummaryReport = () => {

  const [users, Setusers] = useState(userData.slice(0, 50));

  const [pageNumber, setPageNumber]=useState(0);

  const userPerPage = 5;

  const pagesVisited = pageNumber*userPerPage ;

  const displayData = users.slice(pagesVisited, pagesVisited + userPerPage);

  const pageCount = Math.ceil(users.length/userPerPage);

  const changePage = ({selected})=> {
    setPageNumber(selected)
  }

  return (
    <div>
      <h4 style={{marginLeft : '10px' ,color : '#2F4050', marginTop : '15px'}}>Summary Report</h4>
     <StyledTable>
  <TableHead>
    <THead>
    <TableCell>SS Code</TableCell>
    <TableCell>Name</TableCell>
    <TableCell>Zonal Manager</TableCell>
    <TableCell>Region</TableCell>
    <TableCell>Total</TableCell>
    </THead>
  <THead2>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
  </THead2>
  </TableHead>
<StyledBody>
  {
    displayData.map((user, id)=>(
  <TableRow key={user.id}>
    <TableCell>{user.ssCode}</TableCell>
    <TableCell>{user.Name}</TableCell>
    <TableCell>{user.reportingZonalManager}</TableCell>
    <TableCell>{user.region}</TableCell>
    <TableCell>{user.total}</TableCell>
  </TableRow>
  ))
}
</StyledBody>
 
</StyledTable>
<ReactPaginate
previousLabel={'Previous'}
nextLabel={'Next'}
pageCount={pageCount}
onPageChange={changePage}
containerClassName={'paginationBttns'}
previousLinkClassName={'previousBttn'}
nextLinkClassName={'nextBttn'}
disabledClassName={'paginationDisabled'}
activeClassName = {'pagnationActive'}
/>
    </div>
  )
}

export default SummaryReport
     