import { Box, AppBar, Toolbar, Typography, styled } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import url from './exactlyLogo.png';


const Navbar = styled(AppBar)`
   color : #FFFFFF;
   background-color : #FFFFFF;
`
const StyledText = styled(Typography)`
   color : blue;
`;

const Image = styled('img')({
   marginLeft : '20px'
})

const Home = () => {


    return (
        <Box>
            <Navbar postion='static'>
                <Toolbar style={{ minHeight: '40px' }}>

                    <Image src={url} alt='Logo' height='30px' width='100px' />

                    <StyledText style={{ marginLeft: 'auto' }}>Welcome,  Admin</StyledText>

                    <AccountCircleIcon style={{ color: 'black', paddingLeft: '10px'}} />

                </Toolbar>
            </Navbar>

        </Box>


    )
}

export default Home


