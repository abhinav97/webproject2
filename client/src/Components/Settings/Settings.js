import {Table, TableHead, TableRow, TableBody, TableCell, styled} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import {userData} from './SettingData'; 
import {useState} from 'react';
import ReactPaginate from 'react-paginate';
import './setting.css';

const StyledTable= styled(Table)`
marginLeft : '10px';
font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
border :1px solid #dddddd;
margin-top : 20px;
`;

const THead = styled(TableRow)`
    & > th{
    font-size : 18px;
    font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
    font-weight : 600;
    border :1px solid #dddddd;
    }
`;

const THead2 = styled(TableRow)`
& > th{
  font-size : 15px;
  font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
  font-weight : 600;
  border :1px solid #dddddd;
  background-color: #dddddd;
  }
`

const StyledBody  = styled(TableBody)`
    & > tr >td{
      font-size : 15px;
      font-family : 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
      font-weight : 600;
      border :1px solid #dddddd;
    }
      & > tr:nth-child(even) {
      background-color: #dddddd;
    }
`;

const Settings = () => {

  const [users, Setusers] = useState(userData.slice(0, 50));

  const [pageNumber, setPageNumber]=useState(0);

  const userPerPage = 5;

  const pagesVisited = pageNumber*userPerPage ;

  const displayData = users.slice(pagesVisited, pagesVisited + userPerPage);

  const pageCount = Math.ceil(users.length/userPerPage);

  const changePage = ({selected})=> {
    setPageNumber(selected)
  }

  return (
    <div>
      <h4 style={{marginLeft : '10px' ,color : '#2F4050', marginTop : '15px'}}>User List</h4>
     <StyledTable>
  <TableHead>
    <THead>
    <TableCell>Actions</TableCell>
    <TableCell>First Name</TableCell>
    <TableCell>Last Name</TableCell>
    <TableCell>User Name (Login id)</TableCell>
    <TableCell>Report To</TableCell>
    </THead>
  <THead2>
    <TableCell></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
    <TableCell><input type="text" size='10'/></TableCell>
  </THead2>
  </TableHead>
<StyledBody>
  {
    displayData.map((user, id)=>(
  <TableRow key={user.id}>
  
    <TableCell><EditIcon style={{marginRight : '10px'}}/><DeleteIcon/></TableCell>
    <TableCell>{user.fName}</TableCell>
    <TableCell>{user.lName}</TableCell>
    <TableCell>{user.userName}</TableCell>
    <TableCell>{user.reportTo}</TableCell>
  </TableRow>
  ))
}
</StyledBody>
 
</StyledTable>
<ReactPaginate
previousLabel={'Previous'}
nextLabel={'Next'}
pageCount={pageCount}
onPageChange={changePage}
containerClassName={'paginationBttns'}
previousLinkClassName={'previousBttn'}
nextLinkClassName={'nextBttn'}
disabledClassName={'paginationDisabled'}
activeClassName = {'pagnationActive'}
/>
    </div>
  )
}

export default Settings
     