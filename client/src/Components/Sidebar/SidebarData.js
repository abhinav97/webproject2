import InputIcon from '@mui/icons-material/Input';
import VerifiedIcon from '@mui/icons-material/Verified';
import PendingIcon from '@mui/icons-material/Pending';
import PaymentsIcon from '@mui/icons-material/Payments';
import SummarizeIcon from '@mui/icons-material/Summarize';
import SettingsIcon from '@mui/icons-material/Settings';

export const SidebarData = [
    {
        title : 'Entry',
        icon : <InputIcon/>,
        link : '/'
    },
    {
        title : 'Approval List',
        icon : <VerifiedIcon/>,
        link : '/approval'
    },
    {
        title : 'Pending Status',
        icon : <PendingIcon/>,
        link : '/pending'
    },
    {
        title : 'Payment Status',
        icon : <PaymentsIcon/>,
        link : '/payment'
    },
    {
        title : 'Summary',
        icon : <SummarizeIcon/>,
        link : '/summary'
    },
    {
        title : 'Settings',
        icon : <SettingsIcon/>,
        link : '/settings'
    }

]