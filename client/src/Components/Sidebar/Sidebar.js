import './sidebar.css';
import {styled, Box} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import {SidebarData as sidebar} from './SidebarData'; 

const Container = styled(Box)`
  margin-top: 45px;
  display : flex;
  height : 100%;
`

const Sidebar = () => {

  const navigate = useNavigate();

  return (
    <Container>
    <div className='s'>
      <ul className='list'>
      {
      sidebar.map((side, key)=>(
        <li key= {key}
        id={window.location.pathname === side.link ? 'active': ''}
        className='row'
        onClick = {()=>navigate(`${side.link}`)}
        style={{cursor:'pointer'}}
        >
          {" "}
          <div id='icon'>{side.icon}</div>
          <div id='title'>{side.title}</div>
        </li>
      ))
    }
      </ul>
    </div>
    </Container>
  )
}

export default Sidebar